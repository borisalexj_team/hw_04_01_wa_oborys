package com.borisalexj.hw_04_01_wa_oborys;

import android.util.Log;

/**
 * Created by user on 3/30/2017.
 */

public class ImageHolder {
    private static final String TAG = "hw0401" + "ImageHolder";

    private static String[] urls = {"http://stranamasterov.ru/img/i2012/03/17/ku1.jpg",
            "http://stranamasterov.ru/img/i2012/03/17/y1.jpg",
            "http://stranamasterov.ru/img/i2012/03/17/k1.jpg",
            "http://stranamasterov.ru/img/i2012/03/17/untitled_-_39.jpg",
            "http://stranamasterov.ru/img/i2012/03/17/untitled_-_37.jpg"};

    private static int pos = 0;

    public static String getNext() {
        Log.d(TAG, "getNext: " + pos + " " + urls[pos]);
        String result = urls[pos];
        pos++;
        if (pos == urls.length) pos = 0;
        return result;
    }

    public static String getPrev() {
        Log.d(TAG, "getPrev: " + pos + " " + urls[pos]);
        pos--;
        if (pos < 0) pos = urls.length - 1;
        pos--;
        if (pos < 0) pos = urls.length - 1;
        String result = urls[pos];
        pos++;
        if (pos == urls.length) pos = 0;
        return result;
    }

}
