package com.borisalexj.hw_04_01_wa_oborys;

/**
 * Created by user on 3/30/2017.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by borys on 8/28/2016.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    private WeakReference<Callback> mCallback;

    public DownloadImageTask(@NonNull Callback callback) {
        mCallback = new WeakReference<Callback>(callback);
    }

    protected Bitmap doInBackground(String... urls) {
        Bitmap mIcon11 = null;
        try {
            URLConnection connection = new URL(urls[0]).openConnection();
            connection.connect();
            mIcon11 = BitmapFactory.decodeStream(connection.getInputStream());
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
            return null;

        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        Callback callback = mCallback.get();
        if (callback != null) {
            if (result == null) {
                callback.onError();
            } else {
                callback.sendResult(result);
            }
        }
    }

    public interface Callback {
        void sendResult(Bitmap bitmap);

        void onError();
    }
}