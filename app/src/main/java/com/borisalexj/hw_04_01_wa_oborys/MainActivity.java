package com.borisalexj.hw_04_01_wa_oborys;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "hw0401" + this.getClass().getSimpleName();
    private Messenger mService;
    private boolean mIsStarted = false;
    private ImageView mMyImageView;
    private Uri currentUri;
    private BroadcastReceiver broadcastReceiver;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d(TAG, "onServiceConnected: ");

            mService = new Messenger(service);

            if (mIsStarted) {
                sendMessageToService(Constants.MSG_START);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            Log.e(TAG, "onServiceDisconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMyImageView = (ImageView) findViewById(R.id.myImageView);

        if (savedInstanceState != null) {
            currentUri = savedInstanceState.getParcelable("currentUri");
            mMyImageView.setImageURI(currentUri);

            mIsStarted = savedInstanceState.getBoolean("mIsStarted");
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("currentUri", currentUri);
        outState.putBoolean("mIsStarted", mIsStarted);
    }

    private void initializeBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive: inside receiver");
                if (intent.getAction().equals(Constants.newImage)) {
                    currentUri = intent.getParcelableExtra("uri");
                    Log.d(TAG, "onReceive: " + currentUri.toString());
                    mMyImageView.setImageURI(null);
                    mMyImageView.setImageURI(currentUri);
                }

            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.newImage);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeBroadcastReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void navButtonClick(View view) {
        Log.d(TAG, "navButtonClick: ");

        switch (view.getId()) {
            case (R.id.startButton): {
                if (mService != null) {
                    if (mIsStarted) {
                        sendMessageToService(Constants.MSG_STOP);
                    } else {
                        sendMessageToService(Constants.MSG_START);
                    }
                    mIsStarted = !mIsStarted;
                }
                break;
            }
            case (R.id.previousButon):
                sendMessageToService(Constants.MSG_PREVIOUS);
                break;
            case (R.id.nextButton):
                sendMessageToService(Constants.MSG_NEXT);
                break;
        }
    }

    private void sendMessageToService(int message) {
        Message msg = Message.obtain(null, message, 0, 0);
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
        Intent intent = new Intent(this, MyService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        unbindService(mConnection);
        super.onStop();
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        Log.d(TAG, "isMyServiceRunning: ");
        ActivityManager manager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
