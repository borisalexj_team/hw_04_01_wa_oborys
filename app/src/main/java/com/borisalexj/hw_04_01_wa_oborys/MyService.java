package com.borisalexj.hw_04_01_wa_oborys;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user on 3/29/2017.
 */
public class MyService extends Service {
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private final String TAG = "hw0401" + this.getClass().getSimpleName();
    DownloadImageTask.Callback mCallback;
    private Timer timer;

    @Override
    public void onCreate() {
        super.onCreate();
        mCallback = new ServiceCallback(getApplicationContext());
    }

    private void doSomethingRepeatedly() {
        Log.d(TAG, "doSomethingRepeatedly: ");
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Log.d(TAG, "run: ");
                new DownloadImageTask(mCallback).execute(ImageHolder.getNext());
            }
        }, Constants.interval, Constants.interval);
    }

    private void sendUriBroadcast(Uri uri) {
        Log.d(TAG, "sendUriBroadcast: " + this.hashCode());
        Intent intent = new Intent(Constants.newImage);
        intent.putExtra("uri", uri);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: ");
        return mMessenger.getBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (timer != null) {
            timer.cancel();
        }
        return super.onUnbind(intent);
    }

    private class ServiceCallback implements DownloadImageTask.Callback {

        private WeakReference<Context> mContext;

        public ServiceCallback(Context context) {
            mContext = new WeakReference<Context>(context);
        }

        @Override
        public void sendResult(Bitmap bitmap) {
            Context context = mContext.get();
            if (context != null) {
                File file = new File(getCacheDir(), "temp.png");
                if (file.exists()) {
                    file.delete();
                }
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.PNG, 75, fos);
                sendUriBroadcast(Uri.fromFile(file));
            }
        }

        @Override
        public void onError() {
            Log.d(TAG, "onError: ");
        }
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Log.d(TAG, "handleMessage: " + msg.what);
            switch (msg.what) {
                case Constants.MSG_START:
                    doSomethingRepeatedly();
                    break;
                case Constants.MSG_STOP:
                    timer.cancel();
                    break;
                case Constants.MSG_PREVIOUS:
                    new DownloadImageTask(mCallback).execute(ImageHolder.getPrev());
                    break;
                case Constants.MSG_NEXT:
                    new DownloadImageTask(mCallback).execute(ImageHolder.getNext());
                    break;
            }
        }
    }
}
