package com.borisalexj.hw_04_01_wa_oborys;

import java.util.concurrent.TimeUnit;

/**
 * Created by user on 3/29/2017.
 */
public class Constants {
    public static final String newImage = "newImage";
    public static final String nextImage = "nextImage";
    public static final long interval = TimeUnit.SECONDS.toMillis(2);
    static final int MSG_STOP = 0;
    static final int MSG_START = 1;
    static final int MSG_PREVIOUS = 2;
    static final int MSG_NEXT = 3;
}
